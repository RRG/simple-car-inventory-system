# -*- coding: utf-8 -*-
"""
Created on Mon May 31 06:53:33 2021

@author: Rohan
"""
import dash
import dash_bootstrap_components as dbc ##### LIFE SAVER (multi-page enabler)
import dash_html_components as html
import dash_core_components as dcc
import plotly.express as px
from dash.dependencies import Input, Output, State
import pandas as pd
from car_inventory_main import Inventory

inventory_system = Inventory()

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])


# styling the sidebar
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}

# padding for the page content
CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

sidebar = html.Div(                                                             # navigation 
    [
        html.H2("Car Inventory", className="display-5"),
        html.Hr(),
        html.P(
            "Please select an action below to Continue", className="lead"
        ),
        dbc.Nav(
            [
                dbc.NavLink("Display Inventory", href="/", active="exact"),
                dbc.NavLink("Add Car", href="/page-1", active="exact"),
                dbc.NavLink("Edit Car", href="/page-2", active="exact"),
                dbc.NavLink("Delete Car", href="/page-3", active="exact"),
                dbc.NavLink("Graphs", href="/page-4", active="exact")
            ],
            vertical=True,
            pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
)

content = html.Div(id="page-content", children=[], style=CONTENT_STYLE)

app.layout = html.Div([
    dcc.Location(id="url"),
    sidebar,
    content
])

################################## FORM sections ###################################
car_id_input = dbc.FormGroup(
    [
        dbc.Label("Car ID", html_for="car_id_input"),
        dbc.Input(type="string", id="car_id_input", placeholder="Enter car id..."),
        dbc.FormText(
            "Please enter a car id, for eg. 4",
            color="secondary",
        ),
    ]
)

car_name_input = dbc.FormGroup(
    [
        dbc.Label("Car Name", html_for="car_name_input"),
        dbc.Input(type="string", id="car_name_input", placeholder="Enter car name..."),
        dbc.FormText(
            "Please enter a car name, for eg. Perodua",
            color="secondary",
        ),
    ]
)

car_colour_input = dbc.FormGroup(
    [
        dbc.Label("Car Colour", html_for="car_colour_input"),
        dbc.Input(type="string", id="car_colour_input", placeholder="Enter car colour..."),
        dbc.FormText(
            "Please enter a car colour, for eg. Red",
            color="secondary",
        ),
    ]
)

car_kind_input = dbc.FormGroup(
    [
        dbc.Label("Car Kind", html_for="car_kind_input"),
        dbc.Input(type="string", id="car_kind_input", placeholder="Enter car kind..."),
        dbc.FormText(
            "Please enter the kind of car, for eg. SUV",
            color="secondary",
        ),
    ]
)

car_value_input = dbc.FormGroup(
    [
        dbc.Label("Car Value", html_for="car_value_input"),
        dbc.Input(type="string", id="car_value_input", placeholder="Enter car value..."),
        dbc.FormText(
            "Please enter the value of the car, for eg. 12000",
            color="secondary",
        ),
    ]
)

################ Button to kickstart each action -> adding/editing/deleting cars ##########################
submit_butt_add = dbc.Button("Submit", id="submit_action_add",outline=True, color="danger", className="mr-1", block=True)
submit_butt_edit = dbc.Button("Submit", id="submit_action_edit",outline=True, color="danger", className="mr-1", block=True)
submit_butt_del = dbc.Button("Submit", id="submit_action_del",outline=True, color="danger", className="mr-1", block=True)

################ CREATING INPUT FORMS FOR adding/editing/deleting cars ####################################
form_add_car = dbc.Form([car_name_input, car_colour_input,car_kind_input,car_value_input])
form_edit_car = dbc.Form([car_id_input,car_name_input, car_colour_input,car_kind_input,car_value_input])
form_delete_car = dbc.Form([car_id_input])

alert_add = html.Div(                                                          #add car alert html
    [
        html.Hr(),
        dbc.Alert(
            "Car Successfully Added !!!",
            id="but_alert_add",
            is_open=False,
            duration=4000,
        ),
    ]
)

alert_edit = html.Div(                                                         #edit car alert html
    [ 
        html.Hr(),
        dbc.Alert(
            "Car Successfully Edited !!!",
            id="but_alert_edit",
            is_open=False,
            duration=4000,
        ),
    ]
)

alert_del = html.Div(                                                          #delete car alert html
    [
        html.Hr(),
        dbc.Alert(
            "Car Successfully Deleted !!!",
            id="but_alert_del",
            is_open=False,
            duration=4000,
        ),
    ]
)

@app.callback(                                                                 #add car button CALLBACK
    Output("but_alert_add", "is_open"),
    [Input("submit_action_add", "n_clicks")],
    [State("but_alert_add", "is_open"),
     State("url", "pathname"),
     State("car_name_input", "value"),
     State("car_colour_input", "value"),
     State("car_kind_input", "value"),
     State("car_value_input", "value")
     ],
)
def add_car_clicked(btn,is_open,pathname,carName,carColour,carKind,CarValue):
    if btn:
        car_data = carName+','+carColour+','+carKind+','+CarValue
        inventory_system.add_car(car_data)
        return True


@app.callback(                                                                 #edit car button CALLBACK
    Output("but_alert_edit", "is_open"),
    [Input("submit_action_edit", "n_clicks")],
    [State("but_alert_edit", "is_open"),
     State("url", "pathname"),
     State("car_id_input", "value"),
     State("car_name_input", "value"),
     State("car_colour_input", "value"),
     State("car_kind_input", "value"),
     State("car_value_input", "value")
     ],
)
def edit_car_clicked(btn,is_open,pathname,carID,carName,carColour,carKind,CarValue):
    if btn:
        car_data = carName+','+carColour+','+carKind+','+CarValue
        inventory_system.edit_car(carID,car_data)
        return True



@app.callback(                                                                 #delete car button CALLBACK
    Output("but_alert_del", "is_open"),
    [Input("submit_action_del", "n_clicks")],
    [State("but_alert_del", "is_open"),
     State("url", "pathname"),
     State("car_id_input", "value"),
     ],
)
def delete_car_clicked(btn,is_open,pathname,carID):
    if btn:
        inventory_system.delete_car(carID)
        return True


@app.callback(                                                                 #PAGE CHANGE CALLBACK
    Output("page-content", "children"),
    [Input("url", "pathname")]
)
def render_page_content(pathname):
    if pathname == "/":
        return [
                html.H1('Car Inventory Records',
                        style={'textAlign':'center'}),
                dbc.Table.from_dataframe(inventory_system.get_inventory(),
                                         striped=True,
                                         bordered=True, hover=True)
                ]
    elif pathname == "/page-1":
        return [
                html.H1('Add Car to Inventory',
                        style={'textAlign':'center'}),
                form_add_car,submit_butt_add,alert_add
                ]
    elif pathname == "/page-2":
        return [
                html.H1('Edit Car from Inventory',
                        style={'textAlign':'center'}),
               form_edit_car,submit_butt_edit,alert_edit
                ]
    elif pathname == "/page-3":
        return [
                html.H1('Delete Car from Inventory',
                        style={'textAlign':'center'}),
               form_delete_car,submit_butt_del,alert_del
                ]
    elif pathname == "/page-4":
        return [
                html.H1('Car Inventory Graphs',
                        style={'textAlign':'center'}),
                dcc.Graph(
                         figure=px.box(inventory_system.get_inventory(), x="value")
                         ),
                dcc.Graph(
                         figure=px.bar(inventory_system.get_inventory().groupby(by='colour', 
                                                                as_index=False).agg({'Unnamed: 0': pd.Series.nunique}),
                                                                           x='colour',y='Unnamed: 0')
                         ),
                dcc.Graph(
                        figure=px.bar(inventory_system.get_inventory().groupby(by='kind',
                                                                as_index=False).agg({'Unnamed: 0': pd.Series.nunique}),
                                                                           x='kind',y='Unnamed: 0')
                         )
                ]
    # If the user tries to reach a different page, return a 404 message
    return dbc.Jumbotron(
        [
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            html.P(f"The pathname {pathname} was not recognised..."),
        ]
    )


if __name__=='__main__':
    app.run_server(debug=False)
