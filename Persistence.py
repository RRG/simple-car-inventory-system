# -*- coding: utf-8 -*-
"""
Created on Mon Apr 19 09:55:27 2021

@author: Rohan
"""

import csv  #not needed anymore  :)

import pandas as pd
import matplotlib.pyplot as plt

class Vehicle_file():
    
    def writeCarToFile(self,carRow):
        file = pd.read_csv('Inventory.csv',index_col= 0)
        
        new_id = 1                                 
        if file.shape[0] > 0:
            new_id = file.shape[0]                           #calculate new ID of record
        
        data = {
            'name': [carRow[0]], 
            'colour':[carRow[1]],
            'kind':[carRow[2]],
            'value':[carRow[3]]
            }
        
        new_df = pd.DataFrame(data,index=[new_id])
        file = file.append(new_df)
        file.to_csv('Inventory.csv')
    
  
    def readInventory(self):                                                    #display all records of inventory
        file = pd.read_csv('Inventory.csv')
        return file

    

    def deleteCar(self,carId):
        file = pd.read_csv('Inventory.csv',index_col= 0)
        file.drop(index=carId,inplace=True)
        file.reset_index(drop= True,inplace=True)
        file.to_csv('Inventory.csv')                                            #changes made in mem written back to file
    
 
    def updateCar(self,carRow,car_id):
        file = pd.read_csv('Inventory.csv',index_col= 0)
        file.loc[car_id] = carRow
        file.to_csv('Inventory.csv')                                            #changes made in mem written back to file
    
    def plot_bar_count(self,attrToCount):                                       #plot bar chart showing count of records based on an attribute
        file = pd.read_csv('Inventory.csv',index_col= 0)
        plt.rcParams.update({'font.size': 20, 'figure.figsize': (10, 8)})
        fig = file[attrToCount].value_counts().plot(kind = 'bar', rot=0)        #count occurence before plotting
        filename = 'bar_chart_count_{0}.png'.format(attrToCount)
        fig.figure.savefig(filename)                                            #saveplot to png file
        plt.close(fig.figure)                                                   #close plot instance 
        return filename
    
    def plot_box_price(self):                                                   #plot box-and-whisker plot for car price
        file = pd.read_csv('Inventory.csv',index_col= 0)
        plt.rcParams.update({'font.size': 20, 'figure.figsize': (10, 8)})
        fig = file['value'].plot.box()
        filename = 'Price_box_plot.png'
        fig.figure.savefig(filename)                                            #saveplot to png file
        plt.close(fig.figure)                                                   #close plot instance 
        return filename
    

