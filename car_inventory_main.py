# -*- coding: utf-8 -*-
"""
Created on Mon May 24 16:23:31 2021

@author: Rohan
"""

from vehicle_module import Vehicle      #vehicle classs
from Persistence import Vehicle_file    #file handling class
import re                               # REGULAR EXPRESSIONSSSS

class Inventory:
    filesys = Vehicle_file()
    
    def createCarObjFromInput(self,input_str):   #converts user input to vehicle object
        car_details = input_str.split(',')
        car = Vehicle(car_details[0],car_details[1],car_details[2],float(car_details[3]))
        return car
    
    
    def get_inventory(self):
        return Inventory.filesys.readInventory()
    
    
    def add_car(self, car_input):
        car_details = car_input
        car = self.createCarObjFromInput(car_details)
        Inventory.filesys.writeCarToFile(car.getCSVformat())
        print('car added to inventory !')
        
        
        
    def edit_car(self, car_id, car_input):
        car_details = car_input
        Inventory.filesys.updateCar(self.createCarObjFromInput(car_details).getCSVformat(),car_id)
        print('Car record updated !')
        
        
    def delete_car(self,car_id):
        Inventory.filesys.deleteCar(car_id)
        print('Car record deleted !')
        
        
    def display_records(self):
        Inventory.filesys.readInventory()
        
    
    def plot_count(self):
        regexparse = r'colour|kind'
        countAttr = input("Enter car attribute to count (between 'colour' or 'kind'): ")
        input_validation = re.findall(regexparse, countAttr)
        if (len(input_validation) == 0):
                print('Invalid input !')
                return
        else:
            filename =  Inventory.filesys.plot_bar_count(input_validation[0])
            print("plot saved as: {0}".format(filename))
      
        
    def plot_price(self):
       
        filename =  Inventory.filesys.plot_box_price()
        print("plot saved as: {0}".format(filename))


