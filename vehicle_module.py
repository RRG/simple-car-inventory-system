# -*- coding: utf-8 -*-
"""
Created on Mon Apr 19 07:47:48 2021

@author: Rohan
"""

class Vehicle:
    
    def __init__(self,name,color,kind,value):
        self.name = name
        self.kind = kind
        self.color = color
        self.value = value
        
    def set_car_name(self, name):
        self.name = name

    def set_car_kind(self, kind):
        self.kind = kind
        
    def set_car_color(self, color):
        self.color = color
        
    def set_car_value(self, value):
        self.value = value
    
    def description(self):
        desc_str = "%s is a %s %s worth $%.2f." % (self.name, self.color, self.kind, self.value)
        return desc_str
    
    def getCSVformat(self):
        return [self.name, self.color, self.kind, self.value]
   